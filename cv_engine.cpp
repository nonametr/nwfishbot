#include "cv_engine.h"
#include "screen_capture.h"

#include <opencv2/tracking.hpp>

CV_TemplCross_State* CV_Engine::templ_cross_state = nullptr;
CV_TrackCross_State* CV_Engine::track_cross_state = nullptr;
CV_TemplThrow_State* CV_Engine::templ_throw_state = nullptr;
CV_TrackThrow_State* CV_Engine::track_throw_state = nullptr;
CV_TemplWait_State* CV_Engine::templ_wait_state = nullptr;
CV_TrackWait_State* CV_Engine::track_wait_state = nullptr;

CV_Engine::CV_Engine(ScreenCapture& s_cap) : _s_cap(s_cap)
{
    templ_cross_state = new CV_TemplCross_State(s_cap);

    templ_throw_state = new CV_TemplThrow_State(s_cap);
    templ_wait_state = new CV_TemplWait_State(s_cap);

    track_throw_state = new CV_TrackThrow_State();
    track_wait_state = new CV_TrackWait_State();

    _state = templ_cross_state;
}

void CV_Engine::process_frame(Frame& frame)
{
	cv::cvtColor(frame.cvBitmap, frame.cvGrayscale, cv::COLOR_BGR2GRAY);

    _state = _state->process_frame(frame);
}