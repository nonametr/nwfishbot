#pragma once

#include "frame.h"
#include "state_manager/cv_cross_state.h"
#include "state_manager/cv_throw_state.h"
#include "state_manager/cv_wait_state.h"

struct CV_Engine
{
    CV_Engine(struct ScreenCapture& s_cap);

    void process_frame(Frame& frame);

	static CV_TemplCross_State* templ_cross_state;
	static CV_TrackCross_State* track_cross_state;
	static CV_TemplThrow_State* templ_throw_state;
	static CV_TrackThrow_State* track_throw_state;
	static CV_TemplWait_State* templ_wait_state;
	static CV_TrackWait_State* track_wait_state;

private:
	CV_State* _state;
	ScreenCapture& _s_cap;
};
