#pragma once

#include <windows.h>
#include <opencv2/opencv.hpp>

struct Frame
{
    HBITMAP hBitmap;
    cv::Mat cvBitmap;
    cv::Mat cvGrayscale;
};
