#include <iostream>
#include <thread>

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>

#include "cv_engine.h"
#include "screen_capture.h"

const int max_value_H = 360 / 2;
const int max_value = 255;

const cv::String window_name = "test";
const cv::String hsv_window_name = "hsv_test";

int low_H = 0, low_S = 0, low_V = 0;
int high_H = max_value_H, high_S = max_value, high_V = max_value;

static void on_low_H_thresh_trackbar(int, void*)
{
    low_H = min(high_H - 1, low_H);
    cv::setTrackbarPos("Low H", hsv_window_name, low_H);
}
static void on_high_H_thresh_trackbar(int, void*)
{
    high_H = max(high_H, low_H + 1);
    cv::setTrackbarPos("High H", hsv_window_name, high_H);
}
static void on_low_S_thresh_trackbar(int, void*)
{
    low_S = min(high_S - 1, low_S);
    cv::setTrackbarPos("Low S", hsv_window_name, low_S);
}
static void on_high_S_thresh_trackbar(int, void*)
{
    high_S = max(high_S, low_S + 1);
    cv::setTrackbarPos("High S", hsv_window_name, high_S);
}
static void on_low_V_thresh_trackbar(int, void*)
{
    low_V = min(high_V - 1, low_V);
    cv::setTrackbarPos("Low V", hsv_window_name, low_V);
}
static void on_high_V_thresh_trackbar(int, void*)
{
    high_V = max(high_V, low_V + 1);
    cv::setTrackbarPos("High V", hsv_window_name, high_V);
}

int main(int argc, char *argv[])
{
    ScreenCapture s_cap;
    CV_Engine cv_engine(s_cap);    

    cv::namedWindow(window_name);
    cv::namedWindow(hsv_window_name);

    cv::createTrackbar("Low H", hsv_window_name, &low_H, max_value_H, on_low_H_thresh_trackbar);
    cv::createTrackbar("High H", hsv_window_name, &high_H, max_value_H, on_high_H_thresh_trackbar);
    cv::createTrackbar("Low S", hsv_window_name, &low_S, max_value, on_low_S_thresh_trackbar);
    cv::createTrackbar("High S", hsv_window_name, &high_S, max_value, on_high_S_thresh_trackbar);
    cv::createTrackbar("Low V", hsv_window_name, &low_V, max_value, on_low_V_thresh_trackbar);
    cv::createTrackbar("High V", hsv_window_name, &high_V, max_value, on_high_V_thresh_trackbar);

    cv::Mat out1, out2;
    while (true)
    {
        Frame& last_frame = s_cap.getLastFrame();

        cv_engine.process_frame(last_frame);

        cv::cvtColor(last_frame.cvBitmap, out1, cv::COLOR_BGR2HSV);
        cv::inRange(out1, cv::Scalar(low_H, low_S, low_V), cv::Scalar(high_H, high_S, high_V), out2);

        //cv::Mat out;
        //cv::cvtColor(last_frame.cvBitmap, out, cv::COLOR_BGR2GRAY);
        //cv::GaussianBlur(out, out, cv::Size(3, 3), 0);
        
        //cv::Sobel(out, out, -1, 1, 0, 3);
        //cv::Sobel(out, out, -1, 0, 1, 3);
        //cv::Sobel(out, out, -1, 1, 1, 3);

        //cv::Sobel(out, out, 1, 0, 3);
        cv::imshow(window_name, last_frame.cvBitmap);
        //
        cv::imshow(hsv_window_name, out2);
        cv::waitKey(1);
    }

    std::cout << "hello";
    return 1;
}
