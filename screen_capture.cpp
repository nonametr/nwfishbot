#include "screen_capture.h"

constexpr auto TARGET_WINDOW_NAME = "2021-10-14 12-45-12.mkv";

void ScreenCapture::start()
{
    HWND hWnd = ::FindWindow(0, TARGET_WINDOW_NAME);
    RECT r;
    GetWindowRect(hWnd, &r);

    x = r.left;
    y = r.top;
    height = r.bottom - r.top;
    width = r.right - r.left;

    std::thread(&ScreenCapture::capture_thread, this).detach();
    
    while (write_frame - read_frame <= 1)
        std::this_thread::yield();

    read_frame = 1;
}

ScreenCapture::ScreenCapture()
{
    start();
}

void ScreenCapture::capture_thread()
{
    HDC hdc = GetDC(nullptr);
    HDC hDest = CreateCompatibleDC(hdc);

    for (int i = 0; i < MAX_FRAMES; ++i)
    {
        void* ptrBitmapPixels;

        BITMAPINFO bi;
        ZeroMemory(&bi, sizeof(BITMAPINFO));
        bi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
        bi.bmiHeader.biWidth = width;
        bi.bmiHeader.biHeight = -height;
        bi.bmiHeader.biPlanes = 1;
        bi.bmiHeader.biBitCount = 32;

        frame[i].hBitmap = CreateDIBSection(hdc, &bi, DIB_RGB_COLORS, &ptrBitmapPixels, NULL, 0);

        SelectObject(hDest, frame[i].hBitmap);
        frame[i].cvBitmap = cv::Mat(height, width, CV_8UC4, ptrBitmapPixels, 0);
    }

    while(is_running)
    {
        while (write_frame - read_frame >= 2)
            std::this_thread::yield();

        SelectObject(hDest, frame[write_frame++ % MAX_FRAMES].hBitmap);
        BitBlt(hDest, 0, 0, width, height, hdc, x, y, SRCCOPY);

        //cv::imshow("test", matBitmap);
        //cv::waitKey(1);

       // emit frameReady(matBitmap);
//        std::this_thread::sleep_for(std::chrono::milliseconds(25));
    }

    ReleaseDC(NULL, hdc);
    //DeleteObject(hBitmap);
    DeleteDC(hDest);
}


Frame& ScreenCapture::getLastFrame()
{
	while(write_frame - read_frame <= 1)
        std::this_thread::yield();

    return frame[read_frame++ % MAX_FRAMES];
}