#pragma once

#include "frame.h"

constexpr auto MAX_FRAMES = 3;

struct ScreenCapture
{
    ScreenCapture();

    void start();
    void capture_thread();

    Frame& getLastFrame();

    int x = 0;
    int y = 0;
    int width = 0;
    int height = 0;
    
private:
    bool is_running = true;
    Frame frame[MAX_FRAMES];
    uint32_t write_frame = 1;
    uint32_t read_frame = 0;
};
