#include "cv_cross_state.h"

#include "screen_capture.h"

CV_TemplCross_State::CV_TemplCross_State(struct ScreenCapture& s_cap) : _s_cap(s_cap)
{
    float width_scale = _s_cap.width / 1920.0f;
    float height_scale = _s_cap.height / 1080.0f;

    cv::Mat t4_src = cv::imread("C://Users/ikuru/Projects/nwcv/res/im4.jpg");
    cv::Mat t4_mask_src = cv::imread("C://Users/ikuru/Projects/nwcv/res/im4_mask.jpg");

    cv::cvtColor(t4_src, t4_img, cv::COLOR_BGR2GRAY);
    cv::cvtColor(t4_mask_src, t4_mask_img, cv::COLOR_BGR2GRAY);

    cv::resize(t4_img, t4_img, cv::Size(0, 0), width_scale, height_scale, cv::INTER_LINEAR);
    cv::resize(t4_mask_img, t4_mask_img, cv::Size(0, 0), width_scale, height_scale, cv::INTER_LINEAR);
}

CV_State* CV_TemplCross_State::process_frame(Frame& frame)
{
    int match_method = cv::TM_SQDIFF_NORMED;

    cv::Mat r1, r2;
    cv::matchTemplate(frame.cvGrayscale, t4_img, r1, match_method, t4_mask_img);

    double minVal1; double maxVal1;
    cv::Point minLoc1, maxLoc1, matchLoc;
    cv::minMaxLoc(r1, &minVal1, &maxVal1, &minLoc1, &maxLoc1, cv::Mat());

    if (match_method == cv::TM_SQDIFF || match_method == cv::TM_SQDIFF_NORMED)
    {
        matchLoc = minLoc1;
        std::cout << minVal1 << std::endl;
    }
    else
    {
        matchLoc = maxLoc1;
        std::cout << maxVal1 << std::endl;
    }

    if (maxVal1 > 0.95)
    {
        cv::putText(frame.cvBitmap, std::to_string(maxVal1).c_str(), cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        cv::rectangle(frame.cvBitmap, matchLoc, cv::Point(matchLoc.x + t4_img.cols, matchLoc.y + t4_img.rows), CV_RGB(0, 255, 0), 3);
    }
    else
    {
        cv::putText(frame.cvBitmap, "NOT FOUND", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
    }

    return this;
}

CV_TrackCross_State::CV_TrackCross_State()
{
    _tracker = cv::TrackerCSRT::create();
}

void CV_TrackCross_State::init(Frame& frame, cv::Rect&& rect)
{
    _roi = std::move(rect);
    _tracker->init(frame.cvGrayscale, _roi);
}

CV_State* CV_TrackCross_State::process_frame(Frame& frame)
{
    return this;
}