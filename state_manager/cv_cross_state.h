#pragma once

#include "frame.h"
#include "cv_state.h"

#include <opencv2/tracking.hpp>

struct CV_TemplCross_State : public CV_State
{
	CV_TemplCross_State(struct ScreenCapture& s_cap);

	virtual ~CV_TemplCross_State() override = default;

	CV_State* process_frame(Frame& frame) override;

private:
	cv::Mat t4_img;
	cv::Mat t4_mask_img;
	ScreenCapture& _s_cap;
};

struct CV_TrackCross_State : public CV_State
{
	CV_TrackCross_State();

	void init(Frame& frame, cv::Rect&& rect);

	virtual ~CV_TrackCross_State() override = default;

	CV_State* process_frame(Frame& frame) override;

private:
	cv::Rect _roi;
	cv::Ptr<cv::Tracker> _tracker;
};