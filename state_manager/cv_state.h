#pragma once

struct CV_State
{
	virtual ~CV_State() = default;
	virtual CV_State* process_frame(Frame& frame) = 0;
};
