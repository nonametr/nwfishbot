#include "cv_throw_state.h"

#include "screen_capture.h"

CV_TemplThrow_State::CV_TemplThrow_State(struct ScreenCapture& s_cap) : _s_cap(s_cap)
{
    float width_scale = _s_cap.width / 1920.0f;
    float height_scale = _s_cap.height / 1080.0f;

    cv::Mat t3_src = cv::imread("C://Users/ikuru/Projects/nwcv/res/im3.jpg");

    cv::cvtColor(t3_src, t3_img, cv::COLOR_BGR2GRAY);

    cv::resize(t3_img, t3_img, cv::Size(0, 0), width_scale, height_scale, cv::INTER_LINEAR);
}

CV_State* CV_TemplThrow_State::process_frame(Frame& frame)
{
    int match_method = cv::TM_SQDIFF_NORMED;

    cv::Mat r1, r2;
    cv::matchTemplate(frame.cvGrayscale, t3_img, r1, match_method);
    //cv::normalize(r1, r1, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

    double minVal1; double maxVal1;
    cv::Point minLoc1, maxLoc1, matchLoc;
    cv::minMaxLoc(r1, &minVal1, &maxVal1, &minLoc1, &maxLoc1, cv::Mat());

    if (match_method == cv::TM_SQDIFF || match_method == cv::TM_SQDIFF_NORMED)
    {
        matchLoc = minLoc1;
        std::cout << minVal1 << std::endl;
    }
    else
    {
        matchLoc = maxLoc1;
        std::cout << maxVal1 << std::endl;
    }

    if (maxVal1 > 0.95)
    {
        cv::putText(frame.cvBitmap, std::to_string(maxVal1).c_str(), cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        cv::rectangle(frame.cvBitmap, matchLoc, cv::Point(matchLoc.x + t3_img.cols, matchLoc.y + t3_img.rows), CV_RGB(0, 255, 0), 3);
    }
    else
    {
        cv::putText(frame.cvBitmap, "NOT FOUND", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
    }

    return this;
}