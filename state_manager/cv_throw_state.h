#pragma once

#include "frame.h"
#include "cv_state.h"

#include <opencv2/tracking.hpp>

struct CV_TemplThrow_State : public CV_State
{
	CV_TemplThrow_State(struct ScreenCapture& s_cap);

	virtual ~CV_TemplThrow_State() override = default;

	CV_State* process_frame(Frame& frame) override;

private:
	cv::Mat t3_img;
	ScreenCapture& _s_cap;
};

struct CV_TrackThrow_State : public CV_State
{
	CV_TrackThrow_State() = default;

	void init(Frame& frame, cv::Rect&& rect){}

	virtual ~CV_TrackThrow_State() override = default;

	CV_State* process_frame(Frame& frame) override { return this; }

private:
	cv::Rect _roi;
	cv::Ptr<cv::Tracker> _tracker;
};