#include "cv_wait_state.h"

#include "cv_engine.h"
#include "screen_capture.h"

CV_TemplWait_State::CV_TemplWait_State(struct ScreenCapture& s_cap) : _s_cap(s_cap)
{
    float width_scale = _s_cap.width / 1920.0f;
    float height_scale = _s_cap.height / 1080.0f;

    cv::Mat t1_src = cv::imread("C://Users/ikuru/Projects/nwcv/res/im1.jpg");
    cv::Mat t2_src = cv::imread("C://Users/ikuru/Projects/nwcv/res/im2.jpg");

    cv::cvtColor(t1_src, t1_img, cv::COLOR_BGR2GRAY);
    cv::cvtColor(t2_src, t2_img, cv::COLOR_BGR2GRAY);

    cv::resize(t1_img, t1_img, cv::Size(0, 0), width_scale, height_scale, cv::INTER_LINEAR);
    cv::resize(t2_img, t2_img, cv::Size(0, 0), width_scale, height_scale, cv::INTER_LINEAR);
}

CV_State* CV_TemplWait_State::process_frame(Frame& frame)
{
    int match_method = cv::TM_CCORR_NORMED;

    cv::Mat r1, r2;
    cv::matchTemplate(frame.cvGrayscale, t1_img, r1, match_method);
    //cv::normalize(r1, r1, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());

    double minVal1; double maxVal1;
    cv::Point minLoc1, maxLoc1, matchLoc;
    cv::minMaxLoc(r1, &minVal1, &maxVal1, &minLoc1, &maxLoc1, cv::Mat());

    if (match_method == cv::TM_SQDIFF || match_method == cv::TM_SQDIFF_NORMED)
    {
        matchLoc = minLoc1;
        std::cout << minVal1 << std::endl;
    }
    else
    {
        matchLoc = maxLoc1;
        std::cout << maxVal1 << std::endl;
    }

    if (maxVal1 > 0.95)
    {
        cv::putText(frame.cvBitmap, std::to_string(maxVal1).c_str(), cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        cv::rectangle(frame.cvBitmap, matchLoc, cv::Point(matchLoc.x + t2_img.cols, matchLoc.y + t2_img.rows), CV_RGB(0, 255, 0), 3);
    }
    else
    {
        cv::putText(frame.cvBitmap, "NOT FOUND", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
    }

    return this;
    cv::Rect roi = { cv::Point(maxLoc1.x - 24, maxLoc1.y - 24), cv::Point(maxLoc1.x + t1_img.cols + 24 , maxLoc1.y + t1_img.rows + 24) };

    CV_Engine::track_wait_state->init(frame, std::move(roi));
    return static_cast<CV_State*>(CV_Engine::track_wait_state);
}

CV_TrackWait_State::CV_TrackWait_State()
{
    _tracker = cv::TrackerCSRT::create();
}

void CV_TrackWait_State::init(Frame& frame, cv::Rect&& rect)
{
    _roi = std::move(rect);
    _tracker->init(frame.cvGrayscale, _roi);
}

CV_State* CV_TrackWait_State::process_frame(Frame& frame)
{
    static int frame_count = 0;

    bool ok = _tracker->update(frame.cvGrayscale, _roi);
    if (ok)
    {
        putText(frame.cvBitmap, std::to_string(++frame_count).c_str(), cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        cv::rectangle(frame.cvBitmap, _roi, CV_RGB(255, 255, 0), 3);
    }
    else
    {
        putText(frame.cvBitmap, "Tracking failure detected", cv::Point(100, 80), cv::FONT_HERSHEY_SIMPLEX, 0.75, cv::Scalar(0, 0, 255), 2);
        std::exit(EXIT_FAILURE);
    }

    return this;
}