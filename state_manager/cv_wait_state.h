#pragma once

#include "frame.h"
#include "cv_state.h"

#include <opencv2/tracking.hpp>

struct CV_TemplWait_State : public CV_State
{
	CV_TemplWait_State(struct ScreenCapture& s_cap);

	virtual ~CV_TemplWait_State() override = default;

	CV_State* process_frame(Frame& frame) override;

private:
	cv::Mat t1_img;
	cv::Mat t2_img;
	ScreenCapture& _s_cap;
};

struct CV_TrackWait_State : public CV_State
{
	CV_TrackWait_State();

	void init(Frame& frame, cv::Rect&& rect);

	virtual ~CV_TrackWait_State() override = default;

	CV_State* process_frame(Frame& frame) override;

private:
	cv::Rect _roi;
	cv::Ptr<cv::Tracker> _tracker;
};